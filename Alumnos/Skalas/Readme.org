
* Miguel Angel Escalante Serrato

* ¿Quién soy yo? 
+ Matemático y científico de datos. 
+ Software Designer Data Scientist @ [[https://globant.com][Globant]]
+ Presidente @ [[https://sociedat.org][Sociedad de Científicos de Datos de México]]
+ Profesor Licenciatura @ [[https://itam.mx][ITAM]]
